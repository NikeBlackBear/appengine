package com.example.appengine;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserAuthorization extends HttpServlet implements CommonValues {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name = req.getParameter(PARAM_NAME);
		String phoneNumber = req.getParameter(PARAM_PHONE_NUMBER);
		String gcmId = req.getParameter(PARAM_GCM_ID);
		resp.setContentType("text/plain;charset=UTF-8");
		Entity userInfo = new Entity(ENTITY_USER_INFO);
		userInfo.setProperty(PARAM_NAME, name);
		userInfo.setProperty(PARAM_PHONE_NUMBER, phoneNumber);
		userInfo.setProperty(PARAM_GCM_ID, gcmId);
		userInfo.setProperty(PARAM_DIAL_TEXT, "");
		userInfo.setProperty(PARAM_BLOB_KEY, "");
		userInfo.setProperty(PARAM_PROFILE_URL, "");

		DatastoreService dataStore = DatastoreServiceFactory
				.getDatastoreService();
		dataStore.put(userInfo);

		Filter filter = new FilterPredicate(PARAM_PHONE_NUMBER,
				FilterOperator.EQUAL, phoneNumber);
		Query query = new Query(ENTITY_USER_INFO).setFilter(filter);
		// PreparedQuery pq = dataStore.prepare(query);
		// Entity resultEntity = pq.asSingleEntity();
		// Gson gson = new Gson();
		// String json = gson.toJson(lists);
		// JsonParser parser = new JsonParser();
		// JsonArray array = (JsonArray) parser.parse(json);
		// JsonObject obj = (JsonObject) array.get(0);
		// resp.getWriter().println(obj.get("key").getAsJsonObject().get("id"));
		// resp.getWriter().print(resultEntity.getAppId());
		List<Entity> lists = dataStore.prepare(query).asList(
				FetchOptions.Builder.withLimit(1));
		resp.getWriter().print(lists.get(0).getKey().getId());
	}
}