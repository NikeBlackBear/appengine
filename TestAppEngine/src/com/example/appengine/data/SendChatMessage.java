package com.example.appengine.data;

import java.util.List;

import com.example.appengine.CommonValues;

public class SendChatMessage implements CommonValues {
	private String chatRoomId;
	private String chatNo;
	private String sender;
	private String senderName;
	private String messageText;
	private String sendTime;
	private List<String> memberList;
	private String roomType = ROOM_TYPE_EXISTS;

	public void setChatRoomId(String chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getChatRoomId() {
		return chatRoomId;
	}

	public void setChatNo(String chatNo) {
		this.chatNo = chatNo;
	}

	public String getChatNo() {
		return chatNo;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSender() {
		return sender;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setMemberList(List<String> memberList) {
		this.memberList = memberList;
	}

	public List<String> getMemberList() {
		return memberList;
	}

	public void setRoomExist(String roomType) {
		this.roomType = roomType;
	}

	public String getRoomExist() {
		return roomType;
	}
}
