package com.example.appengine.data;

public class Friend {
	private String authId;
	private String name;
	private String dialText;
	private String phoneNumber;
	private String blobKey;
	private String profileUrl;

	public void setAuthId(String id) {
		authId = id;
	}

	public String getAuthId() {
		return authId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDialText(String dial) {
		dialText = dial;
	}

	public String getDialText() {
		return dialText;
	}

	public void setPhoneNumber(String phone) {
		phoneNumber = phone;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setBlobKey(String key) {
		blobKey = key;
	}

	public String getBlobKey() {
		return blobKey;
	}

	public void setProfileUrl(String url) {
		profileUrl = url;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

}
