package com.example.appengine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

@SuppressWarnings("serial")
public class GetImageUploadUrlServlet extends HttpServlet {
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String uploadUrl = blobstoreService.createUploadUrl("/uploads");
		resp.getWriter().print(uploadUrl);
//		final HttpEntity entity = new InputStreamEntity(req.getInputStream(),
//				req.getContentLength());
//
//		MultipartParser parser = new MultipartParser(req, 1024 * 1024);
//		FilePart part = (FilePart) parser.readNextPart();
//		ContentBody body = new InputStreamBody(part.getInputStream(), "choir");
//
//		ByteArrayOutputStream os = new ByteArrayOutputStream();
//		entity.writeTo(os);
//
//		MultipartEntityBuilder mentity = MultipartEntityBuilder.create();
//		mentity.addPart("file", body);
//		mentity.addBinaryBody("file", part.getInputStream());
//		mentity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//
//		HttpClient httpclient = HttpClientBuilder.create().build();
//		HttpPost httppost = new HttpPost(uploadUrl);
//		httppost.setEntity(mentity.build());
//		httppost.addHeader("Content-Type", req.getHeader("Content-Type"));
//		HttpResponse response = null;
//		try {
//			response = httpclient.execute(httppost);
//		} catch (ClientProtocolException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		resp.getWriter().println(httppost);
//
//		 Enumeration e = req.getHeaderNames();
//		 PrintWriter out = resp.getWriter();
//		 while(e.hasMoreElements()){
//		 String name = (String) e.nextElement();
//		 String value = req.getHeader(name);
//		 out.println(name + " = " + value);
//		 }
	}
}
