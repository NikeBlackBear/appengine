package com.example.appengine;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class TestAppEngineServlet extends HttpServlet {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain;charset=UTF-8");
		DatastoreService dataStore = DatastoreServiceFactory
				.getDatastoreService();
		Query query = new Query("Testing");
		List<Entity> lists = dataStore.prepare(query).asList(
				FetchOptions.Builder.withLimit(5));
		Gson gson = new Gson();
		String json = gson.toJson(lists);
		resp.getWriter().println(json);
	}
	
	
	// public void doPost(HttpServletRequest request, HttpServletResponse
	// response)
	// throws IOException {
	// String name = request.getParameter("name");
	// response.setContentType("text/plain;charset=UTF-8");
	// response.getWriter().print(name + " �̸�");
	// Key testBookKey = KeyFactory.createKey("Testing", "test");
	// Entity greeting = new Entity("Testing", testBookKey);
	// greeting.setProperty("name", "ȫ�浿");
	// greeting.setProperty("dialText", "TEST");
	// greeting.setProperty("phoneNumber", "010-4166-6204");
	//
	// DatastoreService dataStore = DatastoreServiceFactory
	// .getDatastoreService();
	// dataStore.put(greeting);
	// }
}