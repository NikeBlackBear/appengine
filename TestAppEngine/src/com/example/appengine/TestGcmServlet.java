package com.example.appengine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.android.gcm.server.*;

public class TestGcmServlet extends HttpServlet {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Sender sender = new Sender("AIzaSyABAkkfzuf2BCK-xw5FpI6fXo4YBwJqZsA");
		Message message = new Message.Builder()
				.collapseKey("1")
				.timeToLive(3)
				.delayWhileIdle(true)
				.addData("message",
						"this text will be seen in notification bar!!").build();
		sender.send(
				message,
				"APA91bHqnvImUgpXxLV_cRa8iEW4KMHzGqSrk2AAOzHlGD96Um6om8e_LHVkOeicjXsIsooAt_bKCGLRK8NxdAzxyUqky_FpgKl-kDGkJTfjcXzQoFogBTHNHZiSNyIxfjCKpmntnHf-NW6IZSp6lcO1sJPHvi_SreNzLPxV6rGPjk7yfN7rmac",
				1);
		resp.getWriter().print("TestGcmServlet :: Complete");
	}
}
