package com.example.appengine;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.appengine.data.SendChatMessage;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

public class SendChatMessageServlet extends HttpServlet implements CommonValues {

	private DatastoreService mDataStore = DatastoreServiceFactory
			.getDatastoreService();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Gson gson = new Gson();
		SendChatMessage preparedMessagePack = gson.fromJson(
				req.getParameter(PARAM_MESSAGE_PACK), SendChatMessage.class);

		if (preparedMessagePack.getChatRoomId().equals("")) {
			Entity chatRoomListEntity = new Entity(ENTITY_CHAT_ROOM_LIST);
			chatRoomListEntity.setProperty(PARAM_CHAT_MEMBER_LIST,
					getAppendedMemberList(preparedMessagePack.getMemberList()));
			Key successkey = mDataStore.put(chatRoomListEntity);
			preparedMessagePack
					.setChatRoomId(String.valueOf(successkey.getId()));
			preparedMessagePack.setRoomExist(ROOM_TYPE_INIT);
		}

		Entity chatListEntity = new Entity(ENTITY_CHAT_LIST);
		chatListEntity.setProperty(PARAM_CHAT_ROOM_ID,
				preparedMessagePack.getChatRoomId());
		chatListEntity.setProperty(PARAM_CHAT_SENDER,
				preparedMessagePack.getSender());
		chatListEntity.setProperty(PARAM_CHAT_SENDER_NAME,
				preparedMessagePack.getSenderName());
		chatListEntity.setProperty(PARAM_CHAT_MESSAGE,
				preparedMessagePack.getMessageText());
		chatListEntity.setProperty(PARAM_SEND_TIME,
				preparedMessagePack.getSendTime());
		chatListEntity.setProperty(PARAM_CHAT_MILLIS,
				System.currentTimeMillis());
		Key chatKey = mDataStore.put(chatListEntity);
		preparedMessagePack.setChatNo(String.valueOf(chatKey.getId()));

		resp.setContentType("text/plain;charset=UTF-8");
		resp.getWriter().print(gson.toJson(preparedMessagePack));

		List<String> receiverList = new ArrayList<String>();

		for (int i = 0; i < preparedMessagePack.getMemberList().size() - 1; i++) {
			Key key = KeyFactory.createKey(ENTITY_USER_INFO,
					Long.parseLong(preparedMessagePack.getMemberList().get(i)));
			Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
					FilterOperator.EQUAL, key);
			Query receiverUserQuery = new Query(ENTITY_USER_INFO)
					.setFilter(filter);
			Entity receiverUser = mDataStore.prepare(receiverUserQuery)
					.asSingleEntity();
			receiverList.add((String) receiverUser.getProperty(PARAM_GCM_ID));
		}

		Sender sender = new Sender(SERVER_GCM_SENDER_ID);
		Message.Builder msgBuilder = new Builder();
		msgBuilder.collapseKey("1");
		msgBuilder.timeToLive(3);
		msgBuilder.delayWhileIdle(true);
		msgBuilder.addData(GCM_EXTRA_MESSAGE, MSG_TYPE_CHAT);
		msgBuilder.addData(PARAM_MESSAGE_PACK,
				URLEncoder.encode(gson.toJson(preparedMessagePack), "EUC-KR"));
		sender.send(msgBuilder.build(), receiverList, 1);
	}

	private String getAppendedMemberList(List<String> memberList) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < memberList.size(); i++) {
			if (i > 0) {
				builder.append("/");
			}
			builder.append(memberList.get(i));
		}
		return builder.toString();
	}
}
