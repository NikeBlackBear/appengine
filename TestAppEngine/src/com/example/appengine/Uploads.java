package com.example.appengine;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.appengine.data.UploadedBlobInfo;
import com.example.appengine.manager.BlobManager;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.api.search.query.ExpressionParser.negation_return;
import com.google.gson.Gson;

public class Uploads extends HttpServlet implements CommonValues {
	private BlobstoreService mBlobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();
	private DatastoreService mDataStore = DatastoreServiceFactory
			.getDatastoreService();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<BlobKey> blobs = mBlobstoreService.getUploads(req).get("file");
		BlobKey blobKey = blobs.get(0);

		if (blobs.size() == 0) {
			resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
			resp.getWriter().print("upload fail. blobs size zero");
		} else {
			String authId = req.getParameter(PARAM_AUTH_ID);
			// Serving URL 생성
			BlobManager blobManager = new BlobManager();
			String servingUrl = blobManager.getServingUrl(blobKey);

			// 이미 저장 된 blobKey 가 있다면 업로드를 위해 파일을 제거 한다
			String pastblobKeyParams = req.getParameter(PARAM_BLOB_KEY);
			if (!pastblobKeyParams.equals("")) {
				BlobKey pastblobKey = new BlobKey(pastblobKeyParams);
				mBlobstoreService.delete(pastblobKey);
			}

			// 프로필 이미지 정보 갱신을 위한 사용자 데이터 쿼리
			Key key = KeyFactory.createKey(ENTITY_USER_INFO,
					Long.parseLong(authId));
			Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
					FilterOperator.EQUAL, key);
			Query newUserQuery = new Query(ENTITY_USER_INFO).setFilter(filter);
			PreparedQuery pq = mDataStore.prepare(newUserQuery);

			// 프로필 이미지 정보 갱신
			Entity userEntity = pq.asSingleEntity();
			userEntity.setProperty(PARAM_BLOB_KEY, blobKey.getKeyString());
			userEntity.setProperty(PARAM_PROFILE_URL, servingUrl);
			mDataStore.put(userEntity);

			UploadedBlobInfo info = new UploadedBlobInfo();
			info.setAuthId(authId);
			info.setBlobKey(blobKey.getKeyString());
			info.setProfileUrl(servingUrl);

			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().print(new Gson().toJson(info));
		}
	}
}