package com.example.appengine.manager;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

public class BlobManager {

	public String getServingUrl(BlobKey blobKey) {
		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		ServingUrlOptions servingOptions = ServingUrlOptions.Builder
				.withBlobKey(blobKey);

		String servingUrl = imagesService.getServingUrl(servingOptions);
		return servingUrl;
	}
}
