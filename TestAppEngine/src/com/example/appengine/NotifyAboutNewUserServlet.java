package com.example.appengine;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//새 사용자 가입 시 연락처와 등록 된 사용자에 매칭 되는 사람들에게 푸쉬 메시지를 보내어 친구 추가가 되도록 하는 Servlet
public class NotifyAboutNewUserServlet extends HttpServlet implements
		CommonValues {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain;charset=UTF-8");
		DatastoreService dataStore = DatastoreServiceFactory
				.getDatastoreService();
		// 새로 가입 한 사용자의 연락처를 받아 온다
		JsonParser parser = new JsonParser();
		JsonArray contactsArry = (JsonArray) parser.parse(req
				.getParameter(PARAM_CONTACTS));

		// DataStore 에 저장 된 가입 된 사용자와 연락처의 데이터를 비교 하여 푸쉬 메시지를 보낼 대상을 리스트화 한다
		Query query = new Query(ENTITY_USER_INFO);
		List<Entity> lists = dataStore.prepare(query).asList(
				FetchOptions.Builder.withLimit(QUERY_LIMIT_COUNT));

		List<String> resultGCMTargetList = new ArrayList<String>();
		for (int i = 0; i < contactsArry.size(); i++) {
			JsonObject contact = contactsArry.get(i).getAsJsonObject();
			String phoneNumber = contact.get(PARAM_PHONE_NUMBER).getAsString()
					.replace("-", "");
			for (int j = 0; j < lists.size(); j++) {
				String registeredPhoneNumber = (String) lists.get(j)
						.getProperty(PARAM_PHONE_NUMBER);
				if (phoneNumber.equals(registeredPhoneNumber)) {
					resultGCMTargetList.add((String) lists.get(j).getProperty(
							PARAM_GCM_ID));
					break;
				}
			}
		}

		// 새 사용자에 대한 정보를 쿼리 한다
		Key key = KeyFactory.createKey(ENTITY_USER_INFO,
				Long.parseLong(req.getParameter(PARAM_AUTH_ID)));
		Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
				FilterOperator.EQUAL, key);
		Query newUserQuery = new Query(ENTITY_USER_INFO).setFilter(filter);
		List<Entity> newUser = dataStore.prepare(newUserQuery).asList(
				FetchOptions.Builder.withLimit(1));

		// 새로 리스트 된 사용자 들에게 새 사용자 정보가 포함 된 가입을 알리는 푸쉬 메시지를 전송 한다.
		Sender sender = new Sender(SERVER_GCM_SENDER_ID);
		Message.Builder msgBuilder = new Builder();
		msgBuilder.collapseKey("1");
		msgBuilder.timeToLive(3);
		msgBuilder.delayWhileIdle(true);
		msgBuilder.addData(GCM_EXTRA_MESSAGE, MSG_TYPE_NEW_USER);
		msgBuilder.addData(PARAM_AUTH_ID,
				String.valueOf(newUser.get(0).getKey().getId()));
		msgBuilder.addData(PARAM_NAME, URLEncoder.encode((String) newUser
				.get(0).getProperty(PARAM_NAME), "EUC-KR"));
		msgBuilder.addData(PARAM_PHONE_NUMBER, (String) newUser.get(0)
				.getProperty(PARAM_PHONE_NUMBER));
		msgBuilder.addData(PARAM_DIAL_TEXT, (String) newUser.get(0)
				.getProperty(PARAM_DIAL_TEXT));
		msgBuilder.addData(PARAM_BLOB_KEY,
				(String) newUser.get(0).getProperty(PARAM_BLOB_KEY));
		msgBuilder.addData(PARAM_PROFILE_URL, (String) newUser.get(0)
				.getProperty(PARAM_PROFILE_URL));

		sender.send(msgBuilder.build(), resultGCMTargetList, 1);
	}
}
