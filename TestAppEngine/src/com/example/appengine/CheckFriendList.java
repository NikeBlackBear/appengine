package com.example.appengine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.appengine.data.Friend;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CheckFriendList extends HttpServlet implements CommonValues {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain;charset=UTF-8");
		DatastoreService dataStore = DatastoreServiceFactory
				.getDatastoreService();
		JsonParser parser = new JsonParser();
		JsonArray contactsArry = (JsonArray) parser.parse(req
				.getParameter(PARAM_CONTACTS));
		Query query = new Query(ENTITY_USER_INFO);
		List<Entity> lists = dataStore.prepare(query).asList(
				FetchOptions.Builder.withLimit(QUERY_LIMIT_COUNT));

		List<Friend> resultFriendList = new ArrayList<Friend>();
		for (int i = 0; i < contactsArry.size(); i++) {
			JsonObject contact = contactsArry.get(i).getAsJsonObject();
			String phoneNumber = contact.get(PARAM_PHONE_NUMBER).getAsString()
					.replace("-", "");
			for (int j = 0; j < lists.size(); j++) {
				String registeredPhoneNumber = (String) lists.get(j)
						.getProperty(PARAM_PHONE_NUMBER);
				if (phoneNumber.equals(registeredPhoneNumber)) {
					Friend friend = new Friend();
					friend.setAuthId(String.valueOf(lists.get(j).getKey()
							.getId()));
					friend.setName((String) lists.get(j)
							.getProperty(PARAM_NAME));
					friend.setPhoneNumber((String) lists.get(j).getProperty(
							PARAM_PHONE_NUMBER));
					friend.setDialText((String) lists.get(j).getProperty(
							PARAM_DIAL_TEXT));
					friend.setBlobKey((String) lists.get(j).getProperty(
							PARAM_BLOB_KEY));
					friend.setProfileUrl((String) lists.get(j).getProperty(
							PARAM_PROFILE_URL));
					resultFriendList.add(friend);
					break;
				}
			}
		}

		resp.getWriter().println(new Gson().toJson(resultFriendList));
	}
}
