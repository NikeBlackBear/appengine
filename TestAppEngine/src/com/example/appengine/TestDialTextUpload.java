package com.example.appengine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class TestDialTextUpload extends HttpServlet implements CommonValues {
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String authId = req.getParameter(PARAM_AUTH_ID);
		String dialText = req.getParameter(PARAM_DIAL_TEXT);
		resp.setContentType("text/plain;charset=UTF-8");

		DatastoreService dataStore = DatastoreServiceFactory
				.getDatastoreService();
		Key key = KeyFactory.createKey(ENTITY_USER_INFO,
				Long.parseLong(req.getParameter(PARAM_AUTH_ID)));
		Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
				FilterOperator.EQUAL, key);
		Query query = new Query(ENTITY_USER_INFO).setFilter(filter);
		PreparedQuery pq = dataStore.prepare(query);
		Entity userInfo = pq.asSingleEntity();
		userInfo.setProperty(PARAM_DIAL_TEXT, dialText);
		dataStore.put(userInfo);

		pq = dataStore.prepare(query);
		userInfo = pq.asSingleEntity();
		String DialTextres = userInfo.getProperty(PARAM_DIAL_TEXT).toString();
		resp.getWriter().print(DialTextres);
	}
}
