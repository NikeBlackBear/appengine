package com.example.appengine;

public interface CommonValues {
	public static final String PARAM_AUTH_ID = "authId";
	public static final String PARAM_NAME = "name";
	public static final String PARAM_PHONE_NUMBER = "phoneNumber";
	public static final String PARAM_GCM_ID = "gcmId";
	public static final String PARAM_CONTACTS = "contacts";
	public static final String PARAM_DIAL_TEXT = "dialText";
	public static final String PARAM_BLOB_KEY = "blobKey";
	public static final String PARAM_PROFILE_URL = "profileUrl";
	public static final String PARAM_MESSAGE_PACK = "messagePack";
	public static final String PARAM_CHAT_ROOM_ID = "chatRoomId";
	public static final String PARAM_CHAT_MEMBER_LIST = "memberList";
	public static final String PARAM_CHAT_SENDER = "sender";
	public static final String PARAM_CHAT_MESSAGE = "messageText";
	public static final String PARAM_SEND_TIME = "sendTime";
	public static final String PARAM_CHAT_SENDER_NAME = "senderName";
	public static final String PARAM_CHAT_MILLIS = "chatMillis";

	public static final String ENTITY_USER_INFO = "user_info";
	public static final String ENTITY_CHAT_ROOM_LIST = "chat_room_list";
	public static final String ENTITY_CHAT_LIST = "chat_list";

	public static final int QUERY_LIMIT_COUNT = 1000;

	public static final String ROOM_TYPE_INIT = "init";
	public static final String ROOM_TYPE_EXISTS = "exists";
	// GCM ����
	public static final String SERVER_GCM_SENDER_ID = "AIzaSyABAkkfzuf2BCK-xw5FpI6fXo4YBwJqZsA";
	public static final String GCM_EXTRA_MESSAGE = "message";
	public static final String MSG_TYPE_NEW_USER = "Detecting New User.";
	public static final String MSG_TYPE_CHAT = "chat_message";
}
